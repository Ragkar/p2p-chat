#!/bin/bash

function print_args {
    echo "Missing arguments. You should give the following argument:"
    echo "  docker -it <docker_image> tracker"
    echo "              OR"
    echo "  docker -it <docker_image> peer <tracker_host> <tracker_port> <chat_username>"
}

if [ $# -lt 1 ]; then
  print_args
  exit 1
fi

case "$1" in
  tracker)
    ./tracker
    ;;
  peer)
    if [ $# -ne 4 ]; then
      print_args
      exit 1
    else
    ./peer $2 $3 $4
    fi
    ;;
esac
