FROM ubuntu:20.04
WORKDIR /app

COPY Makefile run.sh /app/
COPY src/ /app/src
COPY include /app/include

RUN mkdir ./obj
RUN apt-get update
RUN apt-get install -y libboost-system-dev g++ make
RUN make
RUN chmod +x run.sh

ENTRYPOINT ["./run.sh"]
