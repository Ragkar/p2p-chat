# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

# [1.0.0] - 2021-09-10

## Added

- Add Dockerfile for local tests
- Add Makefile
- Add Peer to chat with other Peer
- Add PeerConnection for connection between Peers
- Add Tracker to keep track of connected Peers
- Add TrackerConnection for connection between Tracker and Peer
- Add run.sh used by docker to launch Tracker or Peer
- Add CHANGELOG.md and README.md
