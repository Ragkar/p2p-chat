#pragma once

#include "tracker_connection.hpp"

#include <memory>
#include <unordered_map>

#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>
#include <boost/shared_ptr.hpp>


namespace P2PChat {
  using tcp = boost::asio::ip::tcp;

  class Tracker {
  public:
    /** Tracker constructor.
     *
     * @param[in] ioContext IO Context running the async event loop.
     */
    Tracker(boost::asio::io_context& io_context);

  private:
    /** Accept socket on the port 8080
     *
     * @note It start a async loop accepting socket, ending the with _ioContext
     * event loop.
     */
    void accept();

    /** Callback trigger by `accept`. Send list of peers to the new connected
     * peers, and start checking for socket disconnection.
     *
     * @param[in] newConnection The new connected peer.
     * @param[in] error Error if one has occured.
     */
    void handleAccept(TrackerConnection::ptr newConnection,
        const boost::system::error_code& error);

    /** Get IP address of all connected peers.
     *
     * If their is no connected peer, return "EMPTY\n".
     *
     * @return String containing IP addresses separated with '\n'.
     */
    std::string getPeerList();

    /** Check that each peer's socket is connected. If not, connections are
     * removed.
     */
    void checkPeers();

    /** IO context containing the event loop of the async operation.
     */
    boost::asio::io_context& _ioContext;

    /** ID counter, to give unique identifer to each TrackerConnection.
     */
    int _idCounter;

    /** TCP acceptor waiting for new TrackerConnection.
     */
    tcp::acceptor _acceptor;

    /** Map containing TrackerConnections and their ID.
     */
    std::unordered_map<int, TrackerConnection::ptr> _peers;
  };
}
