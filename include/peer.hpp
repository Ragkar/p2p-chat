#pragma once

#include "peer_connection.hpp"

#include <unordered_map>

#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/enable_shared_from_this.hpp>

namespace P2PChat {
  using tcp = boost::asio::ip::tcp;

  class Peer {
  public:
    /** Peer constructor.
     *
     * @param[in] ioContext IO Context running the async event loop.
     * @param[in] host Address of the tracker server.
     * @param[in] port Port of the tracker server.
     * @param[in] name Name of the peer on the chat.
     */
    Peer(boost::asio::io_context& ioContext,
        const std::string& host,
        const std::string& port,
        const std::string& name);

    /** Send a message to all the connected peer (PeerConnection).
     *
     * @param[in] message Message to broadcast.
     */
    void broadcast(const std::string& message);

    /** Disconnect peer, close all PeerConnection and stop io_context event loop.
     */
    void disconnect();

  private:
    /** Create and connect a socket to the given host and port.
     *
     * @param[in] host Address of remote endpoint to connect.
     * @param[in] port Port of remote endpoint to connect.
     *
     * @return Socket shared pointer connected.
     */
    boost::shared_ptr<tcp::socket> connect(const std::string& host,
        const std::string& port);

    /** Read message from the tracker socket.
     *
     * @note The read is a sync/blocking IO operation.
     *
     * @return Message read from the tracker socket.
     */
    std::string readFromTracker();

    /** Connect to the traker and get list of peers.
     *
     * @param[in] host Address of the tracker server.
     * @param[in] port Port of the tracker server.
     *
     * @return A list of peer IP address, separeted with '\n'.
     */
    std::string connectToTracker(const std::string& host,
        const std::string& port);

    /** Connect to peers, exchange name and start reading messages.
     *
     * @note: For each peer it start an async loop ending with PeerConnection.
     *
     * @param[in] peerAddrs List of peer address separated with '\n'.
     */
    void connectToPeers(const std::string& peerAddrs);

    /** Accept socket on the port 8081
     *
     * @note It start a async loop accepting socket, ending the with _ioContext
     * event loop.
     */
    void accept();

    /** Check that each peer's socket is connected. If not, peers are removed.
     */
    void checkPeers();

    /** Callback trigger by `accept`. Exchange name and start reading messages
     * from the new connected peer.
     *
     * @param[in] newConnection The new connected peer.
     * @param[in] error Error if one has occured.
     */
    void handleAccept(PeerConnection::ptr newConnection,
      const boost::system::error_code& error);

    /** IO context containing the event loop of the async operation.
     */
    boost::asio::io_context& _ioContext;

    /** Name of the current peer on the chat.
     */
    std::string _name;

    /** ID counter, to give unique identifer to each PeerConnection.
     */
    int _idCounter;

    /** TCP socket connected to tracker server.
     */
    boost::shared_ptr<tcp::socket> _tracker;

    /** TCP acceptor waiting for new PeerConnection.
     */
    tcp::acceptor _acceptor;

    /** Map containing PeerConnections and their ID.
     */
    std::unordered_map<int, PeerConnection::ptr> _peers;
  };
}
