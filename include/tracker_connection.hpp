#pragma once

#include <boost/asio.hpp>
#include <boost/enable_shared_from_this.hpp>

namespace P2PChat {
  using tcp = boost::asio::ip::tcp;

  class TrackerConnection : public boost::enable_shared_from_this<TrackerConnection> {
  public:
    using ptr = boost::shared_ptr<TrackerConnection>;

    /** Create on TrackerConnection shared pointer from an io_context.
     *
     * @param[in] ioContext io_context to use in build of the TrackerConnection
     * socket.
     *
     * @return A TrackerConnection shared pointer with a socket that need to be
     * connected.
     */
    static ptr create(boost::asio::io_context& io_context);

    /** Get a reference on the socket used.
     *
     * @return The socket used by the TrackerConnection.
     */
    tcp::socket& socket();

    /** Write a message on the socket.
     *
     * @note The write is an async/non-blocking IO operation.
     *
     * @param[in] message The message to write on the socket.
     */
    void sendMessage(const std::string& message);

    /** Async operation checking if the socket disconnect.
     *
     * @note It use a read which is an async/non-blocking IO operation. Moreover
     * this function is a loop, ending with socket connection.
     */
    void keepConnection();

    /** Get the end point of the remove peer.
     */
    std::string getEndpoint() const;

  private:
    /** Constructor based on io_context.
     *
     * @param[in] ioContext io_context used to initialised the socket.
     *
     * @return A TrackerConnection with a socket that need to be connected.
     */
    TrackerConnection(boost::asio::io_context& io_context);

    /** Empty callback trigger by `sendMessage`.
     */
    void handleWrite(const boost::system::error_code& /*error*/,
        size_t /*bytes_transferred*/);

    /** Callback trigger by `keepConnection`. Close the socket in case of error,
     * call `keepconnection` otherwise.
     *
     * @param[in] error Error if one has occured.
     * @param[in] bytes_transferred Number of bytes read.
     */
    void handleRead(const boost::system::error_code& error,
        size_t bytes_transferred);

    /** TCP socket shared pointer connected to a remote peer.
     */
    tcp::socket _socket;
  };
}
