#pragma once

#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/enable_shared_from_this.hpp>

namespace P2PChat {
  using tcp = boost::asio::ip::tcp;

  class PeerConnection : public boost::enable_shared_from_this<PeerConnection> {
  public:
    using ptr = boost::shared_ptr<PeerConnection>;

    /** Create on PeerConnection shared pointer from an io_context.
     *
     * @param[in] ioContext io_context to use in build of the PeerConnection
     * socket.
     *
     * @return A PeerConnection shared pointer with a socket that need to be
     * connected.
     */
    static ptr create(boost::asio::io_context& ioContext);

    /** Create on PeerConnection shared pointer from an socket shared pointer.
     *
     * @param[in] socket Shared pointer on the socket that will be use in the
     * PeerConnection.
     *
     * @return A usable PeerConnection shared pointer.
     */
    static ptr create(boost::shared_ptr<tcp::socket>& socket);

    /** Get a reference on the socket used.
     *
     * @return The socket used by the PeerConnection.
     */
    tcp::socket& socket();

    /** Write current peer name and read remote peer name from the socket.
     *
     * @note The write/read are sync/blocking IO operation.
     *
     * @param[in] myName Name of the local peer.
     */
    void exchangeName(const std::string& myName);

    /** Write a message on the socket.
     *
     * @note The write is a sync/blocking IO operation.
     *
     * @param[in] message The message to write on the socket.
     */
    void sendMessage(const std::string& message);

    /** Read message on the socket.
     *
     * @note The read is an async/non-blocking IO operation. Moreover this
     * function is a loop, ending with socket connection.
     */
    void readMessage();

    /** Get the name of the remote peer
     *
     * @return Name of the remote peer.
     */
    const std::string& getName();

  private:

    /** Constructor based on io_context.
     *
     * @param[in] ioContext io_context used to initialised the socket.
     *
     * @return A PeerConnection with a socket that need to be connected.
     */
    PeerConnection(boost::asio::io_context& ioContext);

    /** Constructor based on already connected socket.
     *
     * @param[in] socket Socket shared pointer that will be used in the
     * PeerConnection.
     *
     * @return A usable PeerConnection.
     */
    PeerConnection(boost::shared_ptr<tcp::socket>& socket);

    /** Callback trigger by `readMessage`. Write the message read from the
     * socket to the standard output.
     *
     * @param[in] error Error if one has occured.
     * @param[in] bytes_transferred Number of bytes read.
     */
    void handleRead(const::boost::system::error_code& error,
      size_t bytes_transferred);

    /** TCP socket shared pointer connected to a remote peer.
     */
    boost::shared_ptr<tcp::socket> _socket;

    /** Buffer used for the async read on the socket.
     */
    boost::array<char, 4096> _readMessage;

    /** Name of the remote peer on the chat.
     */
    std::string _name;
  };
}
