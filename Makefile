CXX = g++
CXXFLAGS = -Wall -Wpedantic -Werror -std=c++17
LDFLAGS = -I./include
LDLIBS = -lboost_system -pthread

PEER_OBJ_NAME = peer_connection.o peer.o peer_main.o
PEER_OBJ = $(addprefix obj/, $(PEER_OBJ_NAME))

TRACKER_OBJ_NAME = tracker_connection.o tracker.o tracker_main.o
TRACKER_OBJ = $(addprefix obj/, $(TRACKER_OBJ_NAME))

OBJ_TEST_NAME = 
OBJ_TEST = $(addprefix obj/test/, $(OBJ_TEST_NAME))

obj/%.o: src/%.cpp
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $^ -c -o $@

all: tracker peer

tracker: $(TRACKER_OBJ)
	$(LINK.cc) $^ $(LDLIBS) -o $@

peer: $(PEER_OBJ)
	$(LINK.cc) $^ $(LDLIBS) -o $@

runtest: $(OBJ_TEST)
	$(CC) $(CFLAGS) -L./ -lmalloc $^ -o $@
	LD_LIBRARY_PATH=./ ./runtest

clean:
	${RM} ${TRACKER_OBJ} tracker
	${RM} ${PEER_OBJ} peer
