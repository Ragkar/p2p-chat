# P2P Chat

This is a simple experiment on a peer-to-peer chat.

It's written in C++17 with libboost-system 1.71.0.

The project use to binary:

- tracker: it's a server that keep addresses of peers connected to the
  chat. (listen port 8080)
- peer: connect to the server to get peer addresses, then connect
  directly to each peer to exchange messages. (listen port 8081)

# Usage

First thing to do is to run the tracker.

Then you can run peers, that will connect to the tracker to get the
other peer's addresses.

Once the peer connected, you can type your message and click "Enter".
If you want to close the peer, type "exit" and click "Enter".

## Use with docker

You can use docker for local tests. A dockerfile base on ubuntu 20.04
is supply.

To build its image run `docker build -t p2pchat.`

Then you can run containers (and set their ip) as following:

- `docker run -it -ip 172.17.0.2 p2pchat tracker` run tracker with ip `172.17.0.2`
- `docker run -it -ip 172.17.0.3 p2pchat peer 172.17.0.2 8080 foo` run peer
  with ip `172.17.0.3`, connecting to tracker at
  `172.17.0.2:8080` with username `foo`.
