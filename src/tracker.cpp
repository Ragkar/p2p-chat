#include "tracker.hpp"

namespace P2PChat {
  Tracker::Tracker(boost::asio::io_context& ioContext)
      : _ioContext(ioContext)
      , _idCounter(0)
      , _acceptor(ioContext, tcp::endpoint(tcp::v4(), 8080))
      , _peers()
  {
    accept();
  }

  void Tracker::accept()
  {
    TrackerConnection::ptr new_connection = TrackerConnection::create(_ioContext);

    _acceptor.async_accept(
        new_connection->socket(),
        boost::bind(&Tracker::handleAccept, this, new_connection, boost::asio::placeholders::error));
  }

  void Tracker::handleAccept(TrackerConnection::ptr newConnection,
      const boost::system::error_code& error) {

    checkPeers();
    if (!error)
    {
      newConnection->sendMessage(getPeerList());
      _peers.emplace(_idCounter++, newConnection);
      newConnection->keepConnection();
    }

    accept();
  }

  std::string Tracker::getPeerList() {
    std::string buf;
    for (const auto& pair: _peers) {
        buf += pair.second->getEndpoint() + "\n";
    }

    if (buf.empty()) {
      buf = "EMPTY\n";
    }

    return buf;
  }

  void Tracker::checkPeers() {
    std::vector<int> disconnected;
    for (const auto& pair: _peers) {
      if (!pair.second->socket().is_open()) {
        disconnected.push_back(pair.first);
      }
    }

    for (const auto id: disconnected) {
      auto it = _peers.find(id);
      if (it != _peers.cend()) {
        _peers.erase(it);
      }
    }
  }
}
