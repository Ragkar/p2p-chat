#include "peer.hpp"

#include <boost/bind/bind.hpp>

#include <iostream>

namespace P2PChat {
  Peer::Peer(boost::asio::io_context& ioContext,
        const std::string& host,
        const std::string& port,
        const std::string& name)
    : _ioContext(ioContext)
    , _name(name)
    , _idCounter(0)
    , _tracker(nullptr)
    , _acceptor(ioContext, tcp::endpoint(tcp::v4(), 8081))
    , _peers()
  {
    auto peerAddrs = connectToTracker(host, port);
    if (peerAddrs != "EMPTY\n") {
      connectToPeers(peerAddrs);
    }
    std::cout << "*SYSTEM* - info: Connected\n";
    accept();
  }

  boost::shared_ptr<tcp::socket> Peer::connect(const std::string& host,
      const std::string& port) {
    tcp::resolver resolver(_ioContext);
    tcp::resolver::query query(host, port);
    tcp::resolver::results_type endpoints = resolver.resolve(query);

    boost::shared_ptr<tcp::socket> socket(new tcp::socket(_ioContext));
    boost::asio::connect(*socket, endpoints);
    return socket;
  }

  std::string Peer::readFromTracker() {
    boost::system::error_code error;
    boost::array<char, 4096> buf;

    _tracker->wait(tcp::socket::wait_read);
    size_t len = _tracker->read_some(boost::asio::buffer(buf), error);

    if (error) {
        throw boost::system::system_error(error);
    }
    return std::string(buf.data(), len);
  }

  std::string Peer::connectToTracker(const std::string& host, const std::string& port) {
    _tracker = connect(host, port);
    return readFromTracker();
  }

  void Peer::connectToPeers(const std::string& peerAddrs) {
    for (unsigned i = 0; i < peerAddrs.size(); ) {
      auto next = peerAddrs.find_first_of('\n', i);
      std::string addr = peerAddrs.substr(i, next - i);

      auto socket = connect(addr, "8081");
      auto peer = PeerConnection::create(socket);
      peer->exchangeName(_name);
      _peers.emplace(_idCounter++, peer);
      peer->readMessage();

      i = next + 1;
    }
  }

  void Peer::accept() {
    PeerConnection::ptr newConnection = PeerConnection::create(_ioContext);

    _acceptor.async_accept(
        newConnection->socket(),
        boost::bind(&Peer::handleAccept,
          this,
          newConnection,
          boost::asio::placeholders::error));
  }

  void Peer::checkPeers() {
    std::vector<int> disconnected;
    for (const auto& pair: _peers) {
      if (!pair.second->socket().is_open()) {
        disconnected.push_back(pair.first);
      }
    }

    for (const auto id: disconnected) {
      auto it = _peers.find(id);
      if (it != _peers.cend()) {
        _peers.erase(it);
      }
    }
  }

  void Peer::handleAccept(PeerConnection::ptr newConnection,
      const boost::system::error_code& error) {

    if (!error) {
      newConnection->exchangeName(_name);
      std::cout << "*SYSTEM* - info: \"" << newConnection->getName()
        << "\" join the chat.\n";
      _peers.emplace(_idCounter++, newConnection);
      newConnection->readMessage();
    }

    accept();
  }

  void Peer::broadcast(const std::string& message) {
    checkPeers();
    for (const auto& pair: _peers) {
      pair.second->sendMessage(message);
    }
  }

  void Peer::disconnect() {
    _tracker->close();
    for (const auto& pair: _peers) {
      pair.second->socket().close();
    }
    checkPeers();
    _ioContext.stop();
  }
}
