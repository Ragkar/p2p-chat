#include "peer_connection.hpp"

#include <iostream>

#include <boost/bind/bind.hpp>

namespace P2PChat {
  PeerConnection::ptr PeerConnection::create(boost::asio::io_context& ioContext) {
    return PeerConnection::ptr(new PeerConnection(ioContext));
  }

  PeerConnection::ptr PeerConnection::create(boost::shared_ptr<tcp::socket>& socket) {
    return PeerConnection::ptr(new PeerConnection(socket));
  }

  PeerConnection::PeerConnection(boost::asio::io_context& ioContext)
    : _socket(boost::shared_ptr<tcp::socket>(new tcp::socket(ioContext)))
      , _readMessage()
      , _name()
  { }

  PeerConnection::PeerConnection(boost::shared_ptr<tcp::socket>& socket)
    : _socket(socket)
      , _readMessage()
      , _name()
  { }

  tcp::socket& PeerConnection::socket() {
    return *_socket;
  }

  void PeerConnection::exchangeName(const std::string& myName) {
    sendMessage(myName);

    boost::array<char, 128> buf;
    boost::system::error_code error;
    auto len = _socket->read_some(boost::asio::buffer(buf), error);
    _name = std::string(buf.data(), len);
  }

  void PeerConnection::sendMessage(const std::string& message) {
    boost::system::error_code error;
    _socket->write_some(boost::asio::buffer(message), error);
    if (error) {
      throw boost::system::system_error(error);
    }
  }

  void PeerConnection::readMessage() {
    _socket->async_read_some(boost::asio::buffer(_readMessage),
        boost::bind(&PeerConnection::handleRead,
          shared_from_this(),
          boost::asio::placeholders::error,
          boost::asio::placeholders::bytes_transferred));
  }

  const std::string& PeerConnection::getName() {
    return _name;
  }

  void PeerConnection::handleRead(const::boost::system::error_code& error,
      size_t bytes_transferred) {
    if (!error) {
      auto message = std::string(_readMessage.data(), bytes_transferred);
      std::cout << "[" << _name << "] " << message << "\n";
      readMessage();
    } else {
      std::cout << "*SYSTEM* - info: \"" << _name << "\" leave the chat.\n";
      _socket->close();
    }
  }
}
