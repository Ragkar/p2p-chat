#include "peer.hpp"

#include <iostream>

#include <boost/bind/bind.hpp>

int main(int argc, char** argv) {
  if (argc != 4) {
    std::cerr << "usage: " << argv[0] << " <host> <port> <username>\n";
    return EXIT_FAILURE;
  }

  auto host = std::string(argv[1]);
  auto port = std::string(argv[2]);
  auto name = std::string(argv[3]);
  if (name.size() > 100) {
    std::cerr << "*SYSTEM* - ERROR: username must not exceed 100 char.\n";
    return EXIT_FAILURE;
  }

  boost::asio::io_context ioContext;
  P2PChat::Peer peer(ioContext, host, port, name);

  std::thread contextRun(boost::bind(&boost::asio::io_context::run, &ioContext));

  std::string message;
  for (std::getline(std::cin, message); message != "exit"; std::getline(std::cin, message)) {
    peer.broadcast(message);
  }

  peer.disconnect();
  contextRun.join();

  return 0;
}
