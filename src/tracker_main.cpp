#include "tracker.hpp"

#include <iostream>

int main() {
  try {
    boost::asio::io_context ioContext;
    P2PChat::Tracker tracker(ioContext);
    ioContext.run();
  } catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
  }

  return 0;
}
