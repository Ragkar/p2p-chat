#include "tracker_connection.hpp"

#include <boost/array.hpp>
#include <boost/bind/bind.hpp>

namespace P2PChat {
  TrackerConnection::ptr TrackerConnection::create(boost::asio::io_context& io_context)
  {
    return TrackerConnection::ptr(new TrackerConnection(io_context));
  }

  tcp::socket& TrackerConnection::socket()
  {
    return _socket;
  }

  void TrackerConnection::sendMessage(const std::string& message)
  {
    _socket.async_write_some(boost::asio::buffer(message),
        boost::bind(&TrackerConnection::handleWrite,
          shared_from_this(),
          boost::asio::placeholders::error,
          boost::asio::placeholders::bytes_transferred));
  }

  void TrackerConnection::keepConnection()
  {
    boost::array<char, 8> buf;
    _socket.async_read_some(boost::asio::buffer(buf),
        boost::bind(&TrackerConnection::handleRead,
          shared_from_this(),
          boost::asio::placeholders::error,
          boost::asio::placeholders::bytes_transferred));
  }

  std::string TrackerConnection::getEndpoint() const {
    return _socket.remote_endpoint().address().to_string();
  }

  TrackerConnection::TrackerConnection(boost::asio::io_context& io_context)
    : _socket(io_context)
  { }

  void TrackerConnection::handleWrite(const boost::system::error_code& /*error*/,
      size_t /*bytes_transferred*/)
  { }

  void TrackerConnection::handleRead(const boost::system::error_code& error,
      size_t bytes_transferred)
  {
    if (error) {
      _socket.close();
    }

    keepConnection();
  }
}
